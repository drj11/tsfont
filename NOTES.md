## Collected

black jeans/top, black heel boots. Pump Light?
https://i.pinimg.com/originals/7b/f6/37/7bf637d15a3aa7000b1c912dfc344c50.jpg
font: Optima / Optima Medium

Plain white dress, simple outline
https://www.gotceleb.com/wp-content/uploads/photos/taylor-swift/heading-to-the-gym-in-los-angeles/Taylor-Swift-in-White-Mini-Dress--06.jpg
Futura Black Art Deco Outline

gold/snakes/swirls
https://i.pinimg.com/736x/44/a4/8e/44a48e26545dca67102b089278e1694c.jpg
Magnificat

http://images.hellogiggles.com/uploads/2016/11/02220454/Taylor-Swift.jpg
Arnold Böcklin http://www.identifont.com/similar?5YB

https://www.inquirer.com/resizer/szHnjaPCDBnG53Xc4ppY29LK5pE=/1400x0/center/middle/arc-anglerfish-arc2-prod-pmn.s3.amazonaws.com/public/R66IEXIXSVHL5NRATWDRPFR3U4.jpg
Font: Broadway Engraved

Font: https://fontsinuse.com/typefaces/30412/victor-moscoso
Taylor Swift VMA 2019: https://www.hollywoodreporter.com/news/why-taylor-swifts-american-music-awards-outfit-could-be-a-big-statement-1257006

https://www.thebudgetbabe.com/uploads/2016/201609/taylorSwiftPlaidTopBlackAnkleJeans1
Font: Dom Casual

https://www.purseblog.com/images/2016/04/Taylor-Swift-Vetements-Platform-Boots.jpg
Font: Bottleneck

## Needs a font:

https://www.celebritystyleguide.com/images/items/Taylor-swift-pinstripe-blazer.jpg

Glittery short dress
http://assets.teenvogue.com/photos/56f0449b83b45ac83f563b49/master/pass/149629PCNEX_Taylor12.jpeg

vert multicolour stripes
https://4.bp.blogspot.com/--xBlJf30GkI/WjnNC7sWfhI/AAAAAAABJcI/JYgE8bQFgvsT9z0O1tErdeP_i8daHbFvQCLcBGAs/s1600/1474142866-taylor-swift-street-style.jpg

breton stripes
https://celebmafia.com/wp-content/uploads/2016/09/taylor-swift-in-mini-dress-out-in-new-york-09-14-2016-19.jpg

This Is My Fight Song?
https://media1.popsugar-assets.com/files/thumbor/WRJh9kJfF0mfKqpHfLQGBhHEECU/fit-in/1024x1024/filters:format_auto-!!-:strip_icc-!!-/2015/06/17/699/n/1922564/a947c6c02e0767b7_GettyImages-477363476/i/Taylor-close-friend-Selena-Gomez-also-kept-casual-wearing-tight.jpg

knitted: https://i.pinimg.com/originals/d9/cd/0a/d9cd0a39f06f65c7a81a68d0f008cdd6.jpg

curtain/waterfall: https://sassydove.com/wp-content/uploads/2015/07/Taylor-Swift-Gold-Ombre-Fringe-Dress-on-Speak-Now-Tour.jpg

Shadow with hoody: https://images.hellogiggles.com/uploads/2018/05/09131259/taylor-swift-hoodie.jpg

flash-lit rain caught on Reputation stage
https://i.ytimg.com/vi/HpkdDM-KUns/maxresdefault.jpg

One of the Chromey fonts:
https://cdn1.thr.com/sites/default/files/2018/10/gettyimages-1048354630_copy.jpg

dungarees! https://www.heartbowsmakeup.com/wp-content/uploads/2015/09/Best-Taylor-Swift-Fashion-Moments-Photos.jpg
Grotesque 9?

pinstripe
https://www.celebritystyleguide.com/images/items/Taylor-swift-pinstripe-blazer.jpg

dotty dress:
https://www.melissablakeblog.com/wp-content/uploads/2013/06/aa-8.jpg
