In the limelight. Broadway Engraved.
![ts](p11reputation.jpg)
![broadway](p11broadway.png)

Rocking those asymmetric stripes. Taylor Swift and Sinaloa.
![ts](p01ts.jpg)
![sinaloa](p01sinaloa.png)

Taylor Swift inpsiring a generation of psychedelic LP sleeves
using Victor Moscoso.
![Taylor looking fabulous at the VMA](p00VMA2019.png)
![VictorMoscoso](p00VictorMoscoso.png)

Triline means Oxford.
![ts](p05tay.jpg)
![oxford](p05oxford.png)

That font you see on varsity jackets everywhere? TS wore it first.
It's Princetown.
![princetown](p07princetown.png)
![ts](p07shakeitoff.png)

Taylor Swift. Magnificat.
![ts](p12snakes.jpeg)
![magnificat](p12magnificat.png)

Adobe Myriad Pro Bold, as worn by Taylor Swift.
![ts](p02tombradley.jpg)
![myriad](p02myriad.png)

Sometimes you're a whole font, sometimes you're a single letter
from ITC Honda.
![ts](p03vanity.jpeg)
![honda](p03ITC-Honda.png)

Taylor Swift and Arnold Böcklin in elegant noodles.
![ts](p08strappy.jpg)
![ab](p08arnoldbocklin.png)

Stencil on Swift.
![ts](p04taystencil.jpeg)
![stencil](p04stencil.png)

There are many knock-offs, but only one original. Saphir (Zapf, 1953).
![ts](p06tay.jpeg)
![saphir](p06saphir.png)

For when you need to look casual without looking like you need
to look casual: Dom Casual; plaid shirt.
![ts](p09plaid.jpeg)
![domcasual](p09domcasual.png)

Big boots, the perfect match for Bottleneck.
![ts](p10boots.png)
![bottleneck](p10bottleneck.png)

Only the time-machine incident can explain why
the font is called Futura Black Art Deco Outline.
![ts](p13minidress.jpeg)
![ts](p13FuturaBlack.png)

When your outfit is optaymal.
![ts](p14black.jpeg)
![ts](p14optayma.png)
